# Projekt Nimbuspol

## Opis projektu
Projekt obejmuje utworzenie sklepu internetowego o nazwie "Nimbuspol", oferującego produkty z kategorii sprzątania, przechowywania, sztućców i gastronomii.

## Wersja oprogramowania
Uruchamiany jest na maszynie wirtualnej z systemem Ubuntu LTS w wersji 22.04.3 (na stan dzisiejszy).

## Sposób uruchomienia
URL sklepu: [https://10.0.2.15/]

## Skład zespołu
- Piotr F. - scrapper i skrypty
- Wojciech G. - Testy
- Miłosz M. - konfiguracja i zarządzanie repozytorium na GitLab

## Pozostałe informacje
### Informacja o sklepie
- Wersja PrestaShop: 8.1.2 (Pierwsza instalacja na wersji 1.7.8)
- Ścieżka sklepu: /var/www/html

### Konfiguracja poczty
- Metoda obsługi poczty: Własne parametry SMTP
- Nazwa użytkownika SMTP: Zdefiniowany
- Hasło SMTP: Zdefiniowany
- Szyfrowanie: ssl

### Informacje o bazie danych
- Wersja MySQL: 8.0.35-0ubuntu0.22.04.1
- Silnik MySQL: InnoDB
- Sterownik MySQL: DbPDO

### Informacja o serwerze
- Informacje o serwerze: Linux #37~22.04.1-Ubuntu SMP
- Wersja oprogramowania serwera: Apache/2.4.52 (Ubuntu)
- Wersja PHP: 8.1.2-1ubuntu2.14
- Limit pamięci: 256M
- Maksymalny czas wykonywania: 360
- Maksymalny rozmiar pliku do przesłania: 100M
