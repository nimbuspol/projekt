<?php
/* Smarty version 4.3.1, created on 2024-01-08 12:37:37
  from 'module:ps_linklistviewstemplateshooklinkblock.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_659bde81d0a7e5_45166509',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:ps_linklistviewstemplateshooklinkblock.tpl',
      1 => 1704550507,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_659bde81d0a7e5_45166509 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/var/www/html/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/d2/2d/85/d22d851f6d7c12eb35c1c34c8e84baa801a8cea2_2.file.helpers.tpl.php',
    'uid' => 'd22d851f6d7c12eb35c1c34c8e84baa801a8cea2',
    'call_name' => 'smarty_template_function_renderLogo_1314323388659bde81c155d6_85322854',
  ),
));
?><div class="col-md-6 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Produkty</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_1" data-toggle="collapse">
        <span class="h3">Produkty</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_1" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="https://localhost:18720/index.php?controller=prices-drop"
                title="Nasze produkty specjalne"
                            >
              Promocje
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="https://localhost:18720/index.php?controller=new-products"
                title="Nasze nowe produkty"
                            >
              Nowe produkty
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="https://localhost:18720/index.php?controller=best-sales"
                title="Nasze najlepiej sprzedające się produkty"
                            >
              Najczęściej kupowane
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Nasza firma</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_2" data-toggle="collapse">
        <span class="h3">Nasza firma</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_2" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="https://localhost:18720/index.php?id_cms=1&amp;controller=cms"
                title="Warunki dostawy"
                            >
              Dostawa
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="https://localhost:18720/index.php?id_cms=4&amp;controller=cms"
                title=""
                            >
              O nas
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="https://localhost:18720/index.php?controller=contact"
                title="Skorzystaj z formularza kontaktowego"
                            >
              Kontakt z nami
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="https://localhost:18720/index.php?controller=sitemap"
                title="Zagubiłeś się? Znajdź to, czego szukasz!"
                            >
              Mapa strony
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="https://localhost:18720/index.php?controller=stores"
                title=""
                            >
              Sklepy
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }
}
