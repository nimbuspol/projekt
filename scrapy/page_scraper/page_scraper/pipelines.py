# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import re


class ArticleScraperPipeline:

    

    def process_item(self, item, spider):
        DELIMITER = '#'

        adapter = ItemAdapter(item)

        field_names_scrapped = ['name', 'category', 'brand', 'ean13', 'weight', 'quantity', 'description', ]

        for field_name in field_names_scrapped:

            value = adapter.get(field_name)
            if value is not None:
                adapter[field_name] = value.strip()
            else:
                adapter[field_name] = ''

        num_keys = ['price', 'weight']
        for num_key in num_keys:
            value = adapter.get(num_key)
            if(value != ''):
                value = value.replace('\xa0', '')
                value = value.replace(',', '.')
            else:
                value = '0.0'
            adapter[num_key] = float(value)

        quantity_value = adapter.get('quantity')

        if(quantity_value != ''):
            quantity_value = quantity_value.replace(' szt.', '')
        else:
            quantity_value = '0'
        
        quantity_value = int(quantity_value)

        if(quantity_value > 10): quantity_value = 10

        adapter['quantity'] = int(quantity_value)

        image_url_values = adapter.get('image_urls')
        image_urls_string = ''
        for image_url in image_url_values:
            image_urls_string += image_url
            image_urls_string += DELIMITER
        
        image_urls_string = image_urls_string[:-1]
        
        adapter['image_urls'] = image_urls_string
        features = adapter.get('features')
        features_string = ''

        features_string += 'Producent:' + adapter.get('brand') +':1' + DELIMITER

        index = 2

        for feature_key, feature_value in features.items():
            features_string += str(feature_key)
            features_string += str(feature_value).replace(':', '-')
            features_string += ':' + str(index) + DELIMITER
            index += 1

        features_string += 'Waga (kg):' + str(adapter.get('weight')) + ':' + str(index)
        adapter['features'] = features_string


        return item
