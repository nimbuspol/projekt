#!/bin/bash

# update package lists

sudo apt update

# install necessary packages

sudo apy install -y vim
sudo apt install -y unzip
sudo apt install -y git
sudo apt install -y apache2
sudo ufw app list
sudo apt install -y mysql-server
sudo apt install -y php libapache2-mod-php php-mysql

# configure Apache

sudo cp dir.conf /etc/apache2/mods-enabled/dir.conf
sudo service apache2 restart

sudo apt install php8.1-fpm php8.1-common php8.1-mysql php8.1-gmp php8.1-curl php8.1-intl php8.1-mbstring php8.1-xmlrpc php8.1-gd php8.1-xml php8.1-cli php8.1-zip php8.1-imap

# configure PHP

sudo cp php.ini /etc/php/8.1/apache2/php.ini

# database configuration

sudo mysql -uroot -Bse "CREATE DATABASE IF NOT EXISTS prestashop; CREATE USER IF NOT EXISTS 'prestashopuser'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Secure123@'; GRANT ALL PRIVILEGES ON prestashop.* TO 'prestashopuser'@'localhost'; GRANT PROCESS ON *.* TO 'prestashopuser'@'localhost'; FLUSH PRIVILEGES;"

# Prestashop installation / Cloning project repository

sudo rm -r /var/www/
sudo mkdir /var/www/
sudo git clone --branch main --single-branch https://gitlab.com/nimbuspol/projekt.git /var/www/
sudo mysql -u prestashopuser -pSecure123@ prestashop < /var/www/database/prestashop.sql

sudo chown -R www-data:www-data /var/www/html
sudo chmod -R 755 /var/www/html
sudo a2enmod rewrite
sudo systemctl restart apache2

# Configure SSL certificate

sudo ufw allow "Apache Full"
sudo a2enmod ssl
sudo systemctl restart apache2
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt

# Apache site configuration

sudo cp nimbuspol.conf /etc/apache2/sites-available/nimbuspol.conf
sudo a2ensite nimbuspol.conf
sudo apachectl configtest
sudo systemctl reload apache2t SSL

