<?php
/* Smarty version 4.3.1, created on 2024-01-08 12:37:37
  from 'module:ps_customtextps_customtext.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_659bde81a28d86_21262777',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8654b2ba7ef103395c5eb0a4a12ed7463d291bc8' => 
    array (
      0 => 'module:ps_customtextps_customtext.tpl',
      1 => 1704550507,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_659bde81a28d86_21262777 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="custom-text">
  <h2>Ułatw sobie sprzątanie z naszymi produktami!</h2>
<p>Oferujemy <strong>wytrzymałe mopy</strong>, <strong>różnorodne kosze i worki na śmieci</strong>, a także <strong>miotły, zmiotki, szufelki, ścierki, gąbki, czyściki, szczotki i <a href="https://localhost:18720/index.php?id_category=3&amp;controller=category">wiele więcej</a></strong>. Zapraszamy do naszego sklepu, gdzie znajdziesz szeroki wybór produktów do utrzymania czystości. Spraw, aby Twoje otoczenie było zawsze czyste i schludne!</p>
</div>
<?php }
}
