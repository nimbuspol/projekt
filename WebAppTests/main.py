# import libraries
from selenium import webdriver
import config
import time
from methods import *
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options

# make Firefox driver and open browser connected to webapp
main_url = config.ip_address

driver = webdriver.Firefox()
driver.get(main_url)
driver.maximize_window()

start_time = time.time()

add_ten_products(driver)
add_random_product(driver)
delete_from_cart(driver)
register_new_account(driver)
make_order(driver)
check_status(driver)
generate_and_download_invoice(driver)

end_time = time.time()
restult_time = end_time - start_time
print(restult_time)