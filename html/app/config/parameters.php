<?php return array (
  'parameters' => 
  array (
    'database_host' => 'db',
    'database_port' => '',
    'database_name' => 'be_187202_db',
    'database_user' => 'root',
    'database_password' => 'student',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => '9VwGsg3QBG6k2eQ3qnH60vianvXxzXmCV1hukSiWgXn1uGGiDGJgYScXKKRfn0B2',
    'ps_caching' => 'CacheMemcached',
    'ps_cache_enable' => true,
    'ps_creation_date' => '2023-11-15',
    'locale' => 'pl-PL',
    'use_debug_toolbar' => true,
    'cookie_key' => 'zR6mHNNOVMjnpV4C0kRgZFRZ8EG5PcVY3UxsJfDPUrNC6cLXKAwVBAbCAsVJYXO6',
    'cookie_iv' => '5AKFQf7fJWLLupxkdmcBOFW4KBUiLxXp',
    'new_cookie_key' => 'def00000106356e29487c110ee5aadd0f39703d8245edaf8042d7abc14318fc8ebb02ff7c34316c7204fafac3b8d12ceba8357c49999d2ae353c046b4598f155e12e2c5e',
    'api_public_key' => '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2g8KC+3r1pVEhSNBT6Yb
IKrfv03ZME9avyDXBpZ0+HCOk/ESfiMxnPkjWxnaAFXtj8vrnsyTX3RK1ETZXYij
C5ONdE3rKHFEtOuiIMfKhbbQGviXfrX2ZL20RQxOv5oQZ28rtBUG9fh3dQRYsRhB
VPpW/TWx2/9A1tYiNNgumK6rUKTd3FQHLM2yDeRQ2jm0riKPeXCRauIePugrJGAw
2cIqrmx0YPZztdaXiTJLV0gPq48bIlBhR1J5oXGy5q0t/sib7zVPViZhq+hCm3RA
QMuuIy+HaOn38vQRqYC9dTOFN4WvDrg3RA9X8Nhd/gbKFle9xayVMPTxKNn4TF/w
dwIDAQAB
-----END PUBLIC KEY-----
',
    'api_private_key' => '-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDaDwoL7evWlUSF
I0FPphsgqt+/TdkwT1q/INcGlnT4cI6T8RJ+IzGc+SNbGdoAVe2Py+uezJNfdErU
RNldiKMLk410TesocUS066Igx8qFttAa+Jd+tfZkvbRFDE6/mhBnbyu0FQb1+Hd1
BFixGEFU+lb9NbHb/0DW1iI02C6YrqtQpN3cVAcszbIN5FDaObSuIo95cJFq4h4+
6CskYDDZwiqubHRg9nO11peJMktXSA+rjxsiUGFHUnmhcbLmrS3+yJvvNU9WJmGr
6EKbdEBAy64jL4do6ffy9BGpgL11M4U3ha8OuDdED1fw2F3+BsoWV73FrJUw9PEo
2fhMX/B3AgMBAAECggEAVDaGrMC/2elfwwMwj/AYfe8/5gPMowmihDnMkTC+D8x4
Y84wVCKHYJYe/2nmdUa6LlSUtIlRSQYCeCP2xlakkS307TvCW6YS4MQ3ou1F24oU
qLXk2/yuIY3Z9uif6QM43pliiXOtcoT4rWp2Tj6NzKG5wqRgAhR8F/wuzbXHmRL1
P8fI+RC/YQ003D8cgtgCpDK7uUA7WkCPCzgvKzlGQPoRsKvmM1oKUicrdry+LSnA
5yXUONNHC1j3KF3gDAFsgScQT/H3Qafip97sahteeFxajqe7l+rO8kuz7KrcV12e
RuFxqUObMB8NkfmlX4BKRFowjETR3Z0ZMG7YiOxqcQKBgQD4kiSP105CJee9DRmp
l5+uhWYWy/L24Mlk/g3WzZIxh2Kg49Qjp2udSVBZv4BVs1xRZyE28KqXXG+20j/A
Ggum0RPYLVivOge8IWsCfNcxAgQ9xlgkY8owvCea4cm5rHMi+jtDVkO0Y0BLs5DZ
bZLWDNBX5jA6g+q8VD0nItvPdQKBgQDgk3FtwCUj/BHa7ujJgjJlMY/8/y0qm0rR
cxwhh4hAo6Fly9VG9i/Zo250q6cbgp/XvzZ9drHd6vdzRmpSqqGQNxOauOrvfi3b
FwuziWfKU8TCZuRunWe5fM4kCgSk4wzAn6XPhicRx5nOywNFcAya1yIpgSng6YkY
g7IRbrsOuwKBgDw4A32Jdp8V2cd2FgJ9BiVckffx3uGqYYADAE1KriEroFxD9Y/R
roODUGD9wNSCSFo8NtK9iUZ0EWhkFSev56S5QCp+Cd8hmuAb5HXM0sE/sNFabN4c
As69toaSLrG5socFwje0/WAFL31nei8CmbEbl5nD3Wk/ri2D5vDFRCVFAoGATaYt
XglFzj9l3REmJCaETkDozE1ezJCNOgZW/7DhB5iMQapMwBB3UwUTN/vx960en3ZZ
3N+heF252X3LXXHqEvc4LBMsCErUiCBbx2QHIwn7ykvRR/3c02G31PYxzOFEk4Wf
yzVTZO5i3AGNzACGGzOveDJUzJNy96rxct2ah+kCgYAMeYAAQfocH+D9jLMOj8hZ
MbbgCl+uON8VYJbtE51Gt2hsx0lkYAPms8TgdAgIYLjOC87Mu80RkptgCGnmtQ+8
xSkNjDjjawPeGF7V1aNE8/Wip6p0bUBdNUcvjEpdlRTtBHPWLHG6vht7jOWEduxC
1UUH8YCtWXpy6UFIL5T9QQ==
-----END PRIVATE KEY-----
',
  ),
);
