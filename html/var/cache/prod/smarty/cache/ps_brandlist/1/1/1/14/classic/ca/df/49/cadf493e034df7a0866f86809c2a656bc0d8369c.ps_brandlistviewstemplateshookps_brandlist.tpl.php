<?php
/* Smarty version 4.3.1, created on 2024-01-08 12:38:09
  from 'module:ps_brandlistviewstemplateshookps_brandlist.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_659bdea131cdd3_06559985',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ad7605d3e1afaa968ac113b0444601df2cff1153' => 
    array (
      0 => 'module:ps_brandlistviewstemplateshookps_brandlist.tpl',
      1 => 1704550507,
      2 => 'module',
    ),
    '55b14f14c5e6f59d5eb0a3fcbba18cb875222387' => 
    array (
      0 => 'module:ps_brandlistviewstemplates_partialsbrand_text.tpl',
      1 => 1704550507,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_659bdea131cdd3_06559985 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/var/www/html/var/cache/prod/smarty/compile/classiclayouts_layout_left_column_tpl/d2/2d/85/d22d851f6d7c12eb35c1c34c8e84baa801a8cea2_2.file.helpers.tpl.php',
    'uid' => 'd22d851f6d7c12eb35c1c34c8e84baa801a8cea2',
    'call_name' => 'smarty_template_function_renderLogo_926934606659bdea11892c8_57957428',
  ),
));
?>
<div id="search_filters_brands">
  <section class="facet">
          <a href="https://localhost:18720/index.php?controller=manufacturer" class="h6 text-uppercase facet-label" title="producenci">
        Marki
      </a>
    
    <div>
              
<ul>
            <li class="facet-label">
        <a href="https://localhost:18720/index.php?id_manufacturer=52&amp;controller=manufacturer" title="AAA - Polska">
          AAA - Polska
        </a>
      </li>
                <li class="facet-label">
        <a href="https://localhost:18720/index.php?id_manufacturer=40&amp;controller=manufacturer" title="Amefa">
          Amefa
        </a>
      </li>
                <li class="facet-label">
        <a href="https://localhost:18720/index.php?id_manufacturer=19&amp;controller=manufacturer" title="Amefa - Holandia">
          Amefa - Holandia
        </a>
      </li>
                <li class="facet-label">
        <a href="https://localhost:18720/index.php?id_manufacturer=35&amp;controller=manufacturer" title="Amefa Bologna">
          Amefa Bologna
        </a>
      </li>
                <li class="facet-label">
        <a href="https://localhost:18720/index.php?id_manufacturer=4&amp;controller=manufacturer" title="ANEKS">
          ANEKS
        </a>
      </li>
                                                                                                                                                                                                                                                                                    </ul>
          </div>
  </section>
</div>
<?php }
}
