<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerGbSDOkz\appAppKernelProdContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerGbSDOkz/appAppKernelProdContainer.php') {
    touch(__DIR__.'/ContainerGbSDOkz.legacy');

    return;
}

if (!\class_exists(appAppKernelProdContainer::class, false)) {
    \class_alias(\ContainerGbSDOkz\appAppKernelProdContainer::class, appAppKernelProdContainer::class, false);
}

return new \ContainerGbSDOkz\appAppKernelProdContainer([
    'container.build_hash' => 'GbSDOkz',
    'container.build_id' => 'f285474e',
    'container.build_time' => 1704713668,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerGbSDOkz');
