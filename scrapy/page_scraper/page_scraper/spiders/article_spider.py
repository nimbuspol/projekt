import scrapy

from page_scraper.items import ArticleItem

class ArticleSpider(scrapy.Spider):
    name = "article_spider"
    allowed_domains = ["swiat-agd.com.pl"]
    custom_settings = {
        'ITEM_PIPELINES' : {
            "page_scraper.pipelines.ArticleScraperPipeline": 300,
        }
    }
    start_urls = [
        "https://swiat-agd.com.pl/mopy/", 
        "https://swiat-agd.com.pl/kosze-i-worki-na-smieci/", 
        "https://swiat-agd.com.pl/miotly-zmiotki-szufelki/", 
        "https://swiat-agd.com.pl/scierki-i-gabki/", 
        "https://swiat-agd.com.pl/1345-szczotki/", 
        "https://swiat-agd.com.pl/sztucce-amefa/",
        "https://swiat-agd.com.pl/sztucce-gerlach/",
        "https://swiat-agd.com.pl/kosze-i-koszyki/",
        "https://swiat-agd.com.pl/puszki/",
        "https://swiat-agd.com.pl/sloje-i-nakretki/",
        "https://swiat-agd.com.pl/porcelana-cmielow/",
        "https://swiat-agd.com.pl/szklo-krosno/"
    ]


    def parse(self, response):

        articles = response.css('div.product')

        category = response.css('div.cat_name div a ::text')[1].get()

        for article in articles:
            relative_url = article.css('div.prod div.prod_name div.action a.more ::attr(href)').get()

            product_url = 'https://swiat-agd.com.pl/' + relative_url
            
            yield response.follow(product_url, callback = self.parse_product_page, cb_kwargs={'category' : category})
        

        next_page = response.css('div.center a::attr(href)').get()

        if next_page is not None:
            next_page_url = 'https://swiat-agd.com.pl/' + next_page
            yield response.follow(next_page_url, callback = self.parse)

    

    def parse_product_page(self, response, category):

        article_item = ArticleItem()

        image_urls = []
        brand = 'Tuksi'
        weight = ''
        features = {}
        

        table_rows = response.css("table.product_filters tr")
        photo_scrap = response.css('div.desktop_gallery_product a ::attr(href)')
        photo_counter = 3
        if len(photo_scrap) < 3: photo_counter = len(photo_scrap)

        for i in range(photo_counter):
             image_urls.append('https://swiat-agd.com.pl/' + photo_scrap[i].get())

        if (len(table_rows) > 0):

            for row in table_rows:

                if(row.css('th ::text').get() == 'Producent:'):
                    brand = row.css('td ::text').get()
                    

                elif(row.css('th ::text').get() == 'Waga (Kg):'):
                    weight = row.css('td ::text').get().replace(',', '.')

                else:
                    feature = row.css('th ::text').get()
                    feature_value = row.css('td ::text').get()
                    features[feature] = feature_value

        article_item['name'] = response.css('div.product div.product_info div.prod_name h1 ::text').get()
        article_item['category'] = category
        article_item['price'] = response.css('div.price div div.product_price b ::text').get()
        article_item['tax_id'] = 1
        article_item['brand'] = brand
        article_item['ean13'] = response.css('div.params div.parm strong ::text').get()
        article_item['weight'] = weight
        article_item['quantity'] = response.css('div.links ul li.stock span b ::text').get()
        article_item['description'] = response.css('div.desc p::text').get()
        article_item['image_urls'] = image_urls
        article_item['features'] = features

        yield article_item
    