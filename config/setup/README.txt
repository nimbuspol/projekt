Setting Up the Web Project Environment with 'setup.sh'

This guide outlines the step-by-step process to set up a web project environment utilizing Apache, MySQL, PHP, and PrestaShop. 

The setup script may prompt you for certain inputs or passwords during the installation process, especially when setting up MySQL database users and passwords. Ensure you provide necessary inputs as prompted.

Once the setup script completes execution, verify the installation by accessing the project through a web browser. Use the designated URL or IP address associated with your Apache configuration - https://10.0.2.15

Note: The 'setup.sh' script is a powerful tool that automates most of the setup process. Always review the script contents before executing it and ensure it aligns with your project's requirements and security best practices.

Installation Steps

1. Update Package Lists

Ensure your system's package lists are up-to-date.

2. Install Necessary Packages

Install required packages for the project:

    vim: Text editor
    unzip: Tool for extracting files
    git: Version control system
    apache2: Web server
    mysql-server: Database server (MySQL)
    php and related modules: PHP scripting language and its modules

3. Configure Apache

Adjust the Apache configuration as per project requirements. Restart Apache service after making changes.

4. Configure PHP

Modify PHP configuration settings to align with the project's needs.

5. Database Configuration

Set up the MySQL database by creating a database, a user, and granting necessary privileges.

6. PrestaShop Installation

Install PrestaShop by downloading it and setting it up within the appropriate web server directory. This involves cloning a repository from https://www.gitlab.com/nimbuspol/projekt

7. SSL Certificate Configuration

Configure SSL/TLS certificates for secure connections. Ensure appropriate firewall settings for HTTPS.

8. Apache Site Configuration

Set up Apache site configurations, including virtual hosts, to manage the project site effectively.