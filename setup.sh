#!/bin/bash
echo ">>> BAZA DANYCH <<<";
mysql -h $DB_SERVER -u $DB_USER -p$DB_PASSWD -e "drop database if exists $DB_NAME;"
mysql -h $DB_SERVER -u $DB_USER -p$DB_PASSWD -e "create database $DB_NAME";
mysql -h $DB_SERVER -u $DB_USER -p$DB_PASSWD $DB_NAME < /tmp/prestashop.sql

echo ">>> MEMCACHED <<<";
/etc/init.d/memcached start

echo ">>> APACHE <<<";
exec apache2-foreground
