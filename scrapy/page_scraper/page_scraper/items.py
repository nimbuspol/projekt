# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class ArticleItem(scrapy.Item):
    name = scrapy.Field()
    category = scrapy.Field()
    price = scrapy.Field()
    tax_id = scrapy.Field()
    brand = scrapy.Field()
    ean13 = scrapy.Field()
    weight = scrapy.Field()
    quantity = scrapy.Field()
    description = scrapy.Field()
    image_urls = scrapy.Field()
    features = scrapy.Field()

class CategoryItem(scrapy.Item):
    name = scrapy.Field()
    parent_category = scrapy.Field()
