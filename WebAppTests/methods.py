# import libraries
import random
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import config
import time
import string
from random_address import real_random_address

#admin testowy
admin_email = "nimbuspol@wp.pl"
admin_pwd = "nimbuspol.#"
admin_panel_url = config.ip_address + "admin035l3fjhlcirjzvykv9"
main_url = config.ip_address

# zmienne
firstname = ""
lastname = ""
email = ""
password = ""
randomaddress = real_random_address()

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    #print("Random string of length", length, "is:", result_str)
    return result_str

def add_ten_products(driver):
    time.sleep(6)
    try:
        #wejdz w kategorie 1
        driver.find_element(By.ID, "category-3").click()
        print("Odnaleziono i wciśnięto przycisk 'Wszystkie nowe produkty'")
        time.sleep(3)
        elements = driver.find_elements(By.TAG_NAME, "a")
        for element in elements:
            try:
                url = element.get_attribute("href")
                if "W+magazynie" in url:
                    element.click()
                    break
            except NoSuchElementException:
                pass

        time.sleep(3)
        products = driver.find_elements(By.CLASS_NAME, "product-thumbnail")

        for x in reversed(range(len(products)-5)):
            del products[x+4]

        product_links = [product.get_attribute("href") for product in products]
        for product_link in product_links:
           time.sleep(3)

           # otwarcie strony z linku
           driver.get(product_link)
           print("Otwarto stronę")
           quantity_input = driver.find_element(By.ID, "quantity_wanted")
           driver.execute_script("arguments[0].value = '';", quantity_input)
           quantity = random.randint(1,6)
           quantity_input.send_keys(quantity)

           driver.find_element(By.CLASS_NAME, "add-to-cart").click()

           time.sleep(3)
    except NoSuchElementException as e:
        print(f"Błąd: Nie można znaleźć elementu: {e}")

    driver.get(main_url)

    try:
        #wejdz w kategorie 2
        driver.find_element(By.ID, "category-5").click()
        print("Odnaleziono i wciśnięto przycisk 'Wszystkie nowe produkty'")
        time.sleep(3)
        elements = driver.find_elements(By.TAG_NAME, "a")
        for element in elements:
            try:
                url = element.get_attribute("href")
                if "W+magazynie" in url:
                    element.click()
                    break
            except NoSuchElementException:
                pass

        time.sleep(3)
        products = driver.find_elements(By.CLASS_NAME, "product-thumbnail")

        for x in reversed(range(len(products)-5)):
            del products[x+4]

        product_links = [product.get_attribute("href") for product in products]
        for product_link in product_links:
           time.sleep(3)

           # otwarcie strony z linku
           driver.get(product_link)
           print("Otwarto stronę")
           quantity_input = driver.find_element(By.ID, "quantity_wanted")
           driver.execute_script("arguments[0].value = '';", quantity_input)
           quantity = random.randint(1,6)
           quantity_input.send_keys(quantity)

           driver.find_element(By.CLASS_NAME, "add-to-cart").click()

           time.sleep(3)

    except NoSuchElementException as e:
        print(f"Błąd: Nie można znaleźć elementu: {e}")

def add_random_product(driver):
    driver.get(main_url)
    time.sleep(3)
    search_input = driver.find_element(By.CLASS_NAME, "ui-autocomplete-input")
    search_input.send_keys("prymus")
    search_input.send_keys(Keys.ENTER)

    time.sleep(3)
    products = driver.find_elements(By.CLASS_NAME, "product-thumbnail")
    product_links = [product.get_attribute("href") for product in products]
    for product_link in product_links:
        time.sleep(3)
        # otwarcie strony z linku
        driver.get(product_link)
        print("Otwarto stronę")

        try:
            is_not_available = driver.find_element(By.CLASS_NAME, "product-unavailable")
            continue
        except:
            quantity_input = driver.find_element(By.ID, "quantity_wanted")
            driver.execute_script("arguments[0].value = '';", quantity_input)
            quantity = random.randint(1,6)
            quantity_input.send_keys(quantity)
            driver.find_element(By.CLASS_NAME, "add-to-cart").click()
            time.sleep(3)
            break

def delete_from_cart(driver):
    driver.get(main_url)
    cart_button = driver.find_element(By.ID, "_desktop_cart")
    cart_button.click()

    time.sleep(2)
    count_deletes = 0
    while True:
        time.sleep(3)
        if count_deletes >= 3:
            break

        count_deletes += 1

        delete_buttons = driver.find_elements(By.CLASS_NAME, "remove-from-cart")
        delete_buttons[len(delete_buttons)-1].click()

def register_new_account(driver):
    driver.get(main_url + "index.php?controller=registration")
    time.sleep(3)

    firstname = get_random_string(10)
    lastname = get_random_string(10)
    email = get_random_string(10)
    email += "@gmail.com"
    password = get_random_string(10)
    password += "ABC!@"

    random_gender = random.randint(0,1)
    gender_radiuses = driver.find_elements(By.CLASS_NAME, "radio-inline")
    gender_radiuses[random_gender].click()

    time.sleep(1)
    driver.find_element(By.NAME, "firstname").send_keys(firstname)
    time.sleep(1)
    driver.find_element(By.NAME, "lastname").send_keys(lastname)
    time.sleep(1)
    driver.find_element(By.NAME, "email").send_keys(email)
    time.sleep(1)
    driver.find_element(By.NAME, "password").send_keys(password)
    time.sleep(1)

    driver.find_element(By.NAME, "optin").click()
    time.sleep(1)
    driver.find_element(By.NAME, "psgdpr").click()
    time.sleep(1)
    driver.find_element(By.NAME, "newsletter").click()
    time.sleep(1)
    driver.find_element(By.NAME, "customer_privacy").click()
    time.sleep(1)

    driver.find_element(By.CLASS_NAME, "form-control-submit").click()

def make_order(driver):
    time.sleep(6)
    driver.get(main_url + "index.php?controller=cart&action=show")
    time.sleep(3)

    driver.find_element(By.CLASS_NAME, "cart-detailed-actions").click()

    time.sleep(2)

    driver.find_element(By.NAME, "address1").send_keys(randomaddress['address1'])
    time.sleep(1)

    driver.find_element(By.NAME, "address2").send_keys(randomaddress['address2'])
    time.sleep(1)

    randomaddress['postalCode'] = randomaddress['postalCode'][:2] + "-" + randomaddress['postalCode'][2:]
    driver.find_element(By.NAME, "postcode").send_keys(randomaddress['postalCode'])
    time.sleep(1)

    driver.find_element(By.NAME, "city").send_keys(randomaddress['city'])
    time.sleep(1)

    driver.find_element(By.NAME, "id_country").click()
    time.sleep(1)
    country_options = driver.find_elements(By.TAG_NAME, "option")
    for country in country_options:
        country_text = country.text
        if "Polska" in country_text:
            country.click()
            time.sleep(1)
            break

    driver.find_element(By.NAME, "confirm-addresses").click()
    time.sleep(3)

    random_delivery = random.randint(1,2)
    delivery_options = driver.find_elements(By.CLASS_NAME, "delivery-option")
    delivery_options[random_delivery].click()
    time.sleep(1)

    random_delivery_message = get_random_string(20)
    driver.find_element(By.ID, "delivery_message").send_keys(random_delivery_message)
    time.sleep(2)
    driver.find_element(By.NAME, "confirmDeliveryOption").click()
    time.sleep(2)

    payment_option = "payment-option-2"
    print(payment_option)
    labels = driver.find_elements(By.TAG_NAME, "label")
    for label in labels:
        attrib = label.get_attribute("for")
        if attrib == payment_option:
            label.click()
            break
    time.sleep(2)

    driver.find_element(By.ID, "conditions_to_approve[terms-and-conditions]").click()
    time.sleep(2)
    button_div = driver.find_element(By.ID, "payment-confirmation")
    button_div.find_element(By.TAG_NAME, "button").click()

def check_status(driver):
    time.sleep(7)
    driver.find_element(By.CLASS_NAME, "account").click()
    time.sleep(2)
    driver.find_element(By.ID, "history-link").click()
    time.sleep(5)

def generate_and_download_invoice(driver):
    driver.get(admin_panel_url)
    time.sleep(5)

    admin_email_input = driver.find_element(By.NAME, "email")
    admin_password_input = driver.find_element(By.NAME, "passwd")

    admin_email_input.send_keys(admin_email)
    time.sleep(2)
    admin_password_input.send_keys(admin_pwd)
    admin_password_input.send_keys(Keys.ENTER)
    time.sleep(5)

    order_rows = driver.find_elements(By.CLASS_NAME, "column-reference")
    order_rows[0].click()

    time.sleep(5)
    a_items_list_to_hide_toolbar = driver.find_elements(By.TAG_NAME, "a")
    for item_a in a_items_list_to_hide_toolbar:
        try:
            attrib = item_a.get_attribute("title")
            if attrib == "Close Toolbar":
                item_a.click()
                break
        except NoSuchElementException:
            pass

    time.sleep(7)
    nav_items_list = driver.find_element(By.ID, "orderDocumentsTab")
    nav_items_list.click()

    time.sleep(5)
    generate_buttons = driver.find_elements(By.CLASS_NAME, "material-icons")
    for button in generate_buttons:
        button_text = button.text
        if "autorenew" in button_text:
            button.click()
            break
    time.sleep(5)
    nav_items_list = driver.find_element(By.ID, "orderDocumentsTab")
    nav_items_list.click()

    time.sleep(3)
    a_items_list = driver.find_elements(By.TAG_NAME, "a")
    for item in a_items_list:
        item_text = item.text
        if "#FV" in item_text:
            item.click()
            break