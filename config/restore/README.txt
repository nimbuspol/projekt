Restore Script Guide

The 'restore.sh' script facilitates the restoration of project settings from a GitLab repository. Before running this script, ensure that the following configurations are set up:

1. Apache Configuration

    Installation and Activation:
        Apache (apache2) should be installed and running on your system.
        Enable the necessary modules such as rewrite to support URL rewriting.

2. PHP and PHP Modules

    PHP Installation:
        Ensure PHP (php) is installed with required modules like php-mysql, php-gd, php-curl, etc., matching the project's specifications.

3. MySQL Database

    Database Setup:
        MySQL server (mysql-server) should be installed and running.
        Create a database (prestashop) and a user (prestashopuser) with appropriate privileges to access the database.

4. Git and Project Repository

    Git Configuration:
        Git (git) should be installed to clone the project repository from GitLab.

5. Project Directory and Permissions

    Web Directory:
        Ensure the /var/www/ directory exists and has the necessary permissions (www-data:www-data) to accommodate the project files.

The script performs the following actions:

    Deletes the existing backup if it exists (~/backup/).
    Copies the contents of /var/www/ (project files) into the backup directory.
    Clones the project repository from the GitLab repository into /var/www/.
    Imports the PrestaShop database (prestashop.sql) into MySQL.
    Sets appropriate permissions and ownership for the web directory.
    Enables Apache rewrite module and restarts Apache.

Important Note

    Caution: Running this script will remove the contents of /var/www/ and replace them with the latest version from the GitLab repository. Ensure you have a backup of any critical data before executing this script.

    Post-Backup: After running this script, verify the functionality of your project by accessing it through a web browser to ensure it's working as expected.

