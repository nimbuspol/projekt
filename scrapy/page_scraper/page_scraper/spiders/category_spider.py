import scrapy

from page_scraper.items import CategoryItem

class CategorySpiderSpider(scrapy.Spider):
    name = "category_spider"
    allowed_domains = ["swiat-agd.com.pl"]
    custom_settings = {
        'CONCURRENT_REQUESTS' : '1'
    }

    start_urls = [
        # GŁÓWNE KATEGORIE
        "https://swiat-agd.com.pl/pranie-i-sprzatanie/",
        "https://swiat-agd.com.pl/przechowywanie/",
        "https://swiat-agd.com.pl/sztucce/",
        "https://swiat-agd.com.pl/gastronomia/",

        # PODKATEGORIE
        "https://swiat-agd.com.pl/mopy/", 
        "https://swiat-agd.com.pl/kosze-i-worki-na-smieci/", 
        "https://swiat-agd.com.pl/miotly-zmiotki-szufelki/", 
        "https://swiat-agd.com.pl/scierki-i-gabki/", 
        "https://swiat-agd.com.pl/1345-szczotki/", 
        "https://swiat-agd.com.pl/sztucce-amefa/",
        "https://swiat-agd.com.pl/sztucce-gerlach/",
        "https://swiat-agd.com.pl/kosze-i-koszyki/",
        "https://swiat-agd.com.pl/puszki/",
        "https://swiat-agd.com.pl/sloje-i-nakretki/",
        "https://swiat-agd.com.pl/porcelana-cmielow/",
        "https://swiat-agd.com.pl/szklo-krosno/"
    ]

    def parse(self, response):

        category_item = CategoryItem()

        categories = response.css('div.cat_name div a ::text')

        name = ''
        parent_category = ''

        if len(categories) > 1:
            name = categories[1].get()
            parent_category = categories[0].get()
        else:
            name = categories[0].get()
            parent_category = ''
        
        category_item['name'] = name
        category_item['parent_category'] = parent_category

        yield category_item
