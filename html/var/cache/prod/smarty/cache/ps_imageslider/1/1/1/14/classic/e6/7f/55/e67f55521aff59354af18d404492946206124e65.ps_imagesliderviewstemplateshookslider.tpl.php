<?php
/* Smarty version 4.3.1, created on 2024-01-08 12:37:37
  from 'module:ps_imagesliderviewstemplateshookslider.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_659bde81a0cf32_48538375',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6c2108a17c7103b6e203f4f0621d4645b56b0114' => 
    array (
      0 => 'module:ps_imagesliderviewstemplateshookslider.tpl',
      1 => 1704550507,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_659bde81a0cf32_48538375 (Smarty_Internal_Template $_smarty_tpl) {
?>
  <div id="carousel" data-ride="carousel" class="carousel slide" data-interval="5000" data-wrap="true" data-pause="hover" data-touch="true">
    <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
          </ol>
    <ul class="carousel-inner" role="listbox" aria-label="Pokaz slajdów">
              <li class="carousel-item active" role="option" aria-hidden="false">
          <a href="https://localhost:18720/index.php?controller=new-products">            <figure>
              <img src="https://localhost:18720/modules/ps_imageslider/images/0546e9672d8ce13c8ef856ca2fbc7d0162b58b11_baner1.png" alt="Nowości" loading="lazy" width="1110" height="340">
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Nowości</h2>
                  <div class="caption-description"><h3>Niedawno dodane produkty</h3>
<p>Zapraszamy do zapoznania się z naszymi nowym asortymentem!</p></div>
                </figcaption>
                          </figure>
          </a>        </li>
              <li class="carousel-item " role="option" aria-hidden="true">
          <a href="https://localhost:18720/index.php?controller=best-sales">            <figure>
              <img src="https://localhost:18720/modules/ps_imageslider/images/c96d13a3caadd1db0f6939f24175b3874300c88b_baner3.png" alt="S" loading="lazy" width="1110" height="340">
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Prawdziwie magiczne towary</h2>
                  <div class="caption-description"><p>Zajrzyj do najchętniej kupowanych rzeczy z naszego sklepu!</p></div>
                </figcaption>
                          </figure>
          </a>        </li>
              <li class="carousel-item " role="option" aria-hidden="true">
          <a href="https://localhost:18720/index.php?id_category=6&amp;controller=category">            <figure>
              <img src="https://localhost:18720/modules/ps_imageslider/images/3a9df965a482baa99fabc7aec6f67c3e25635442_baner2.png" alt="Wyjątkowa porcelana i nie tylko!" loading="lazy" width="1110" height="340">
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Nie tylko sprzątanie...</h2>
                  <div class="caption-description"><p>Sprawdź nasze produkty dotyczące gastronomii!</p></div>
                </figcaption>
                          </figure>
          </a>        </li>
          </ul>
    <div class="direction" aria-label="Przyciski karuzeli">
      <a class="left carousel-control" href="#carousel" role="button" data-slide="prev" aria-label="Poprzedni">
        <span class="icon-prev hidden-xs" aria-hidden="true">
          <i class="material-icons">&#xE5CB;</i>
        </span>
      </a>
      <a class="right carousel-control" href="#carousel" role="button" data-slide="next" aria-label="Następny">
        <span class="icon-next" aria-hidden="true">
          <i class="material-icons">&#xE5CC;</i>
        </span>
      </a>
    </div>
  </div>
<?php }
}
