+------------------------------------------ How to use ------------------------------------------+
|
| 1. Best way to look at script and run it is by using PyCharm Community software
| 2. Open project in PyCharm, you will get notification to install requirements.txt
|    Or you can install it manually in terminal with `pip install -r requirements.txt`
| 3. Open config.py and change ip_address of your webpage host
| 4. In PyCharm open main.py and run script by Shift + F10
|
| If you get some errors be sure you have installed the lastest version of geckodriver
| You can find the lastest version on github.com/mozilla/geckodriver/releases
| In project I have using version 0.33.0
| To install it open terminal and type:
| wget https://github.com/mozilla/geckodriver/releases/download/v0.33.0/geckodriver-v0.33.0-linux64.tar.gz
| tar -xvzf geckodriver*
| chmod +x geckodriver
| sudo mv geckodriver /usr/local/bin/ or sudo mv geckodriver /usr/bin/
|
| When running script you get error from Firefox about Profile:
| sudo snap remove firefox
| sudo add-apt-repository ppa:mozillateam/ppa
| echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001
' | sudo tee /etc/apt/preferences.d/mozilla-firefox
|
|
| echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
|
| sudo apt install firefox
|
+------------------------------------------ How to use ------------------------------------------+
