FROM prestashop/prestashop:1.7.8

RUN apt-get update
RUN apt-get install -y vim

RUN a2enmod ssl
RUN service apache2 restart
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt -subj "/C=PL/ST=Pomorskie/L=Gdansk/O=PG/OU=Informatyka/CN=localhost/emailAddress=nimbuspol@wp.pl"
COPY ./apache2.conf /etc/apache2/apache2.conf
COPY ./nimbuspol.conf /etc/apache2/sites-available/nimbuspol.conf
RUN a2ensite nimbuspol.conf
RUN apachectl configtest

RUN rm -rf /var/www && mkdir /var/www && mkdir /var/www/html

WORKDIR /var/www/html

COPY --chown=www-data:www-data html/ ./
RUN chmod -R 755 /var/www/html
RUN a2enmod rewrite

COPY ./setup.sh /tmp/
COPY ./database/prestashop.sql /tmp/

RUN apt-get update && apt-get install -y vim && apt-get install -y libz-dev libmemcached-dev && \
    apt-get install -y memcached libmemcached-tools && \
    pecl install memcached && \
    docker-php-ext-enable memcached 
    
ENTRYPOINT ["/tmp/setup.sh"]

EXPOSE 443
